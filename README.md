# My personal Neovim configuration files

This is the result of my attempt to tame Neovim via modular Lua-only
configuration.
I took quite a lot inspiration from the excellent
[Neovim from scratch](https://github.com/LunarVim/Neovim-from-scratch).

## Requirements

The following dependencies have to be installed to make the configuration work:

- neovim
- [Nerd Font](https://www.nerdfonts.com) variant of Fira Code ([Install Nerd
  Font on Arch Linux](https://www.behova.net/install-nerd-font-on-arch-linux)).
- `unzip`, `nodejs` and `npm` for installing certain plugins.
- ctags if tagbar is used.
- jq if nvim-jqx is be used.
- yq if nvim-jqx is be used.
- Lua 5.1 or LuaJIT, see [Neorg Prerequisites](https://github.com/nvim-neorg/neorg/wiki/Kickstart#prerequisites)

## Getting started

1. Start neovim.
2. Call `:checkhealth` to see what is missing, e.g. programmes which are not
   yet installed.
3. If Java development is relevant, some details in most likely need to be
   adjusted `ftplugin/lua/user/plugins/lsp/java.lua`.

## TODO

- [~] Setup debugging infrastructure.
- [x] Leave a star at each used plugin.
  - [x] LunarVim/Neovim-from-scratch
  - [x] wbthomason/packer.nvim
  - [x] lewis6991/impatient.nvim
  - [x] nvim-tree/nvim-tree.lua
  - [x] nvim-tree/nvim-web-devicons
  - [x] nvim-lualine/lualine.nvim
  - [x] glepnir/lspsaga
  - [x] goolord/alpha-nvim
  - [x] numToStr/Comment.nvim
  - [x] lukas-reineke/indent-blankline.nvim
  - [x] windwp/nvim-autopairs
  - [x] nvim-lua/plenary.nvim
  - [x] kylechui/nvim-surround
  - [x] preservim/tagbar
  - [x] nvim-telescope/telescope.nvim
  - [x] nvim-telescope/telescope-fzf-native.nvim
  - [x] nvim-lua/popup.nvim
  - [x] BurntSushi/ripgrep
  - [x] sharkdp/fd
  - [x] folke/lazy.nvim
  - [x] folke/trouble.nvim
  - [x] folke/todo-comments.nvim
  - [x] folke/which-key.nvim
  - [x] folke/zen-mode.nvim
  - [x] williamboman/mason.nvim
  - [x] williamboman/mason-lspconfig.nvim
  - [x] neovim/nvim-lspconfig
  - [x] simrat39/rust-tools.nvim
  - [x] lewis6991/gitsigns.nvim
  - [x] j-hui/fidget.nvim
  - [x] hrsh7th/nvim-cmp
  - [x] hrsh7th/cmp-buffer
  - [x] hrsh7th/cmp-nvim-lua
  - [x] hrsh7th/cmp-nvim-lsp
  - [x] hrsh7th/cmp-nvim-lsp-signature-help
  - [x] hrsh7th/cmp-path
  - [x] f3fora/cmp-spell
  - [x] onsails/lspkind.nvim
  - [x] saadparwaiz1/cmp_luasnip
  - [x] rafamadriz/friendly-snippets
  - [x] nvim-treesitter/nvim-treesitter
  - [x] mfussenegger/nvim-dap
  - [x] rcarriga/nvim-dap-ui
  - [x] theHamsta/nvim-dap-virtual-text
  - [x] mfussenegger/nvim-lint
  - [x] numToStr/FTerm.nvim
  - [x] rakr/vim-one
  - [x] tjdevries/colorbuddy.vim
  - [x] Th3Whit3Wolf/onebuddy
  - [x] Th3Whit3Wolf/one-nvim
  - [x] EdenEast/nightfox.nvim
  - [x] navarasu/onedark.nvim
  - [x] adisen99/codeschool.nvim
  - [x] rktjmp/lush.nvim
  - [x] phaazon/hop.nvim
- [] Add support for LaTeX.
- [] Try out p00f/nvim-ts-rainbow
