local onedark_status, onedark = pcall(require, "onedark")
if (not onedark_status) then
    return
end

local function get_theme_overrides(style)
    local palette = require "onedark.palette"
    local palette_style = palette[style]
    if (not palette_style) then
        vim.notify(
            string.format("Could not get palette for style %q.", style),
            vim.log.levels.ERROR
        )
        return
    end

    local colour_bg0
    if ("darker" == style) then
        colour_bg0 = "#000000"
    elseif ("light" == style) then
        colour_bg0 = "#ffffff"
    else
        colour_bg0 = palette_style["bg0"]
    end

    return {
        colors = {
            bg0 = colour_bg0,
        },
    }
end

local default_style = "darker"

-- Automatically apply theme overrides on style toggle
vim.api.nvim_create_autocmd(
    "ColorSchemePre",
    {
        group = vim.api.nvim_create_augroup("OnedarkStyle", { clear = true }),
        desc = "Apply theme overrides before colour scheme change",
        callback = function()
            local onedark_config = vim.g.onedark_config
            if onedark_config and onedark_config.loaded then
                local overrides = get_theme_overrides(onedark_config.style)
                if (not overrides) then
                    return
                end
                onedark.set_options("colors", overrides.colors)
            end
        end
    }
)

onedark.setup({
    toggle_style_list = { "light", "darker" },
    style = default_style,
    colors = get_theme_overrides(default_style).colors,
})
onedark.load()

-- Set key binding for toggling style
local which_key_status, which_key = pcall(require, "which-key")
if (not which_key_status) then
    return
end

which_key.add({
    { "<leader>to", onedark.toggle, desc = "Toggle OneDark theme style" },
})
