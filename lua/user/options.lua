local opt = vim.opt

opt.colorcolumn = "80"
opt.cursorline = true
opt.cursorlineopt = "number,line"
opt.guifont = "FiraCode Nerd Font Mono:h11" -- The font used in graphical Neovim applications.
opt.mouse = "a"                             -- Allow the mouse to be used in Neovim.
opt.number = true
opt.relativenumber = true
opt.scrolloff = 8      -- Minimal number of screen lines to keep above and below the cursor.
opt.shortmess:append "c"
opt.showmode = false   -- Do not show things like -- INSERT --.
opt.sidescrolloff = 8
opt.signcolumn = "yes" -- Always show the sign column, otherwise it would shift the text each time.
opt.spelllang = "en,de"
opt.spellsuggest = "best,9"
opt.spellfile = "juergen.utf-8.add"
opt.splitbelow = true
opt.termguicolors = true -- True colours
opt.updatetime = 300     -- Faster completion (4000 ms default).
opt.timeoutlen = 250
opt.title = true

-- Indentation
opt.autoindent = true
opt.expandtab = true -- Convert tabs to spaces.
opt.shiftwidth = 4   -- The number of spaces inserted for each indentation.
opt.smartindent = true
opt.tabstop = 4      -- Amount of spaces to insert for a tab.
opt.wrap = false     -- Display lines as one long line.

-- Folding using Treesitter
--opt.foldmethod = "expr"
--opt.foldexpr = "nvim_treesitter#foldexpr()"

-- Folding settings for nvim-ufo
opt.foldcolumn = '1' -- '0' is not bad
opt.foldlevel = 99   -- Using ufo provider need a large value, feel free to decrease the value
opt.foldlevelstart = 99
opt.foldenable = true

-- Highlighted yank
vim.api.nvim_create_autocmd(
    { "TextYankPost" },
    {
        pattern = { "*" },
        callback = function()
            vim.highlight.on_yank({ higroup = "IncSearch", timeout = 700 })
        end,
        group = vim.api.nvim_create_augroup("highlight_yank", {}),
    }
)
