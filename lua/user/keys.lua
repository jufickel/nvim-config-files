-------------------------
-- Global key mappings --
-------------------------
local which_key = require "which-key"

which_key.add({
    { "<leader>n", "<Cmd>nohlsearch<CR>",              desc = "Hide search highlight" },
    { "<leader>q", "<Cmd>q<CR>",                       desc = "Quit" },
    { "<leader>w", "<Cmd>w<CR>",                       desc = "Save" },
    { "<leader>W", "<Cmd>wa<Bar>exe 'mksession!'<CR>", desc = "Save all and create a session" },
    -- Better window navigation
    { "<C-h>",     "<C-w>h",                           desc = "Go to the left window" },
    { "<C-j>",     "<C-w>j",                           desc = "Go to the down window" },
    { "<C-l>",     "<C-w>l",                           desc = "Go to the right window" },
    -- Navigate buffers
    { "<S-h>",     "<Cmd>bprev<CR>",                   desc = "Previous buffer" },
    { "<S-l>",     "<Cmd>bnext<CR>",                   desc = "Next buffer" },
    { "<F11>",     "<Cmd>set spell!<CR>",              desc = "Spell checking" },
    -- Exclusive up and down for wrapped lines
    { "k",         "gk",                               desc = "Exclusive up" },
    { "j",         "gj",                               desc = "Exclusive down" },
})

which_key.add({
    -- Shortcut for ESC
    { "jk", "<Esc>", desc = "Shortcut for ESC", mode = "i" },
    { "kj", "<Esc>", desc = "Shortcut for ESC", mode = "i" },
})

which_key.add(
    {
        mode = { "t" },
        { "<C-Esc>", "<C-\\><C-n>",       desc = "Escape terminal mode" },
        { "<C-w>h",  "<C-\\><C-n><C-w>h", desc = "Escape terminal to left window" },
        { "<C-w>l",  "<C-\\><C-n><C-w>l", desc = "Escape terminal to right window" },
        { "<C-w>j",  "<C-\\><C-n><C-w>j", desc = "Escape terminal to below window" },
        { "<C-w>k",  "<C-\\><C-n><C-w>k", desc = "Escape terminal to above window" },
    }
)

-- Square bracket movements for diagnostics
local diagnostic_goto = function(next, severity)
    local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
    severity = severity and vim.diagnostic.severity[severity] or nil
    return function()
        go({ severity = severity })
    end
end

which_key.add(
    {
        { "]d", diagnostic_goto(true),           desc = "Next diagnostic", },
        { "[d", diagnostic_goto(false),          desc = "Previous diagnostic", },
        { "]e", diagnostic_goto(true, "ERROR"),  desc = "Next error", },
        { "[e", diagnostic_goto(false, "ERROR"), desc = "Previous error", },
        { "]w", diagnostic_goto(true, "WARN"),   desc = "Next warning", },
        { "[w", diagnostic_goto(false, "WARN"),  desc = "Previous warning", },
    }
)

-- Automatically append semicolon at line end
vim.api.nvim_set_keymap(
    "i",
    "<A-;>",
    "<Esc>A;<Esc>",
    { desc = "Append semicolon", noremap = true, silent = true }
)
vim.api.nvim_set_keymap(
    "n",
    "<A-;>",
    "A;<Esc>",
    { desc = "Append semicolon", noremap = true, silent = true }
)

-- [Remapping all your textobjs for speed](https://nanotipsforvim.prose.sh/remapping-all-your-textobjs-for-speed)
vim.api.nvim_set_keymap(
    "o",
    "ar",
    "a]",
    { desc = "Rectangular bracket (included)" }
) -- [r]ectangular bracket
vim.api.nvim_set_keymap(
    "o",
    "ir",
    "i]",
    { desc = "Rectangular bracket (excluded)" }
) -- [r]ectangular bracket
vim.api.nvim_set_keymap(
    "o",
    "ac",
    "a}",
    { desc = "Curly brace (included)" }
) -- [c]urly brace
vim.api.nvim_set_keymap(
    "o",
    "ic",
    "i}",
    { desc = "Curly brace (excluded)" }
) -- [c]urly brace
vim.api.nvim_set_keymap(
    "o",
    "am",
    "aW",
    { desc = "Massive word" }
) -- [m]assive word
vim.api.nvim_set_keymap(
    "o",
    "aq",
    'a"',
    { desc = "Quote (included)" }
) -- [q]uote
vim.api.nvim_set_keymap(
    "o",
    "iq",
    'i"',
    { desc = "Quote (excluded)" }
) -- [q]uote
vim.api.nvim_set_keymap(
    "o",
    "az",
    "a'",
    { desc = "Single quote (included)" }
) -- [z]ingle quote
vim.api.nvim_set_keymap(
    "o",
    "iz",
    "i'",
    { desc = "Single quote (excluded)" }
) -- [z]ingle quote
