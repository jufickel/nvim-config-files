local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"
local gradle_ls_pkg_name = "gradle-language-server"
if (mason_registry.ensure_installed(gradle_ls_pkg_name)) then
    local cmp_lsp = require "cmp_nvim_lsp"

    -- Enable (broadcasting) snippet capability for completion
    lspconfig.gradle_ls.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
