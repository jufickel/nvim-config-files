local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"
local bash_ls_pkg_name = "bash-language-server"
if (mason_registry.ensure_installed(bash_ls_pkg_name)) then
    if (not mason_registry.ensure_installed("shellcheck")) then
        print("Shellcheck is missing. Please provide an installation.")
    end

    local cmp_lsp = require "cmp_nvim_lsp"

    -- Enable (broadcasting) snippet capability for completion
    lspconfig.bashls.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
