local M = {}

function M.ensure_installed(package_name)
    local mason_registry_status, mason_registry = pcall(require, "mason-registry")
    if (not mason_registry_status) then
        return false, nil
    end

    if (not mason_registry.is_installed(package_name)) then
        print("Installing " .. package_name)
        local package = mason_registry.get_package(package_name)
        package:install({})
    end

    return mason_registry.is_installed(package_name)
end

return M
