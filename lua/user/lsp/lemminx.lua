local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"
local lemminx_pkg_name = "lemminx"
if (mason_registry.ensure_installed(lemminx_pkg_name)) then
    local cmp_lsp = require "cmp_nvim_lsp"
    lspconfig.lemminx.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
