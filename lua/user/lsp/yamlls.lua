local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"
local yaml_ls_pkg_name = "yaml-language-server"
if (mason_registry.ensure_installed(yaml_ls_pkg_name)) then
    local cmp_lsp = require "cmp_nvim_lsp"

    lspconfig.yamlls.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
