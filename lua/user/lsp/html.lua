local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"
local html_ls_pkg_name = "html-lsp"
if (mason_registry.ensure_installed(html_ls_pkg_name)) then
    local cmp_lsp = require "cmp_nvim_lsp"
    lspconfig.html.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
