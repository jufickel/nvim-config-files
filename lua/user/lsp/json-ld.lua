local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"
local jsonld_ls_pkg_name = "jsonld-lsp"
if (mason_registry.ensure_installed(jsonld_ls_pkg_name)) then
    local configs = require "lspconfig.configs"
    configs.jsonld = {
        default_config = {
            cmd = { "jsonld-language-server" },
            filetypes = { "jsonld" },
            root_dir = require("lspconfig.util").find_git_ancestor,
            single_file_support = true,
            init_options = {},
        }
    }


    local cmp_lsp = require "cmp_nvim_lsp"
    lspconfig.jsonld.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
