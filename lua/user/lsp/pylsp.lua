local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"
local python_ls_pkg_name = "python-lsp-server"
if (mason_registry.ensure_installed(python_ls_pkg_name)) then
    local cmp_lsp = require "cmp_nvim_lsp"

    -- Enable (broadcasting) snippet capability for completion
    lspconfig.pylsp.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
