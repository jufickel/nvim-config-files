local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    vim.notify("lspconfig not found.", vim.log.levels.WARN, {})
    return
end

local mason_registry = require "user.lsp.mason-registry"
local tailwind_pkg_name = "tailwindcss-language-server"
if (mason_registry.ensure_installed(tailwind_pkg_name)) then
    local cmp_lsp = require "cmp_nvim_lsp"
    local tailwindcss_config = lspconfig.tailwindcss
    tailwindcss_config.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
