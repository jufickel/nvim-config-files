local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"

-- LSP
local kotlin_ls_pkg_name = "kotlin-language-server"
if (mason_registry.ensure_installed(kotlin_ls_pkg_name)) then
    local cmp_lsp = require "cmp_nvim_lsp"

    -- Enable (broadcasting) snippet capability for completion
    lspconfig.kotlin_language_server.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end

local dap_status, dap = pcall(require, "dap")
if (not dap_status) then
    return
end

-- DAP
local kotlin_dap_pkg_name = "kotlin-debug-adapter"
if (mason_registry.ensure_installed(kotlin_dap_pkg_name)) then
    dap.adapters.kotlin = {
        type = "executable",
        command = vim.fn.stdpath("data") .. "/mason/bin/kotlin-debug-adapter",
        args = { "--interpreter=vscode" },
    }

    dap.configurations.kotlin = {
        {
            type = "kotlin",
            name = "launch - kotlin",
            request = "launch",
            projectRoot = vim.fn.getcwd() .. "/app",
            mainClass = function()
                return vim.fn.input("Path to main class > ", "", "file")
            end,
        },
    }
end
