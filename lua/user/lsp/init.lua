-- LSP clients
require "user.lsp.bash-lsp"
require "user.lsp.gradle"
require "user.lsp.html"
require "user.lsp.json-ld"
require "user.lsp.jsonls"
require "user.lsp.kotlin"
require "user.lsp.lemminx"
require "user.lsp.lua_ls"
require "user.lsp.marksman"
require "user.lsp.pylsp"
require "user.lsp.spectral-language-server"
require "user.lsp.tailwindcss"
require "user.lsp.ts-ls"
require "user.lsp.yamlls"

-- Show diagnostic popup on cursor hover
-- vim.api.nvim_create_autocmd(
--     "CursorHold",
--     {
--         callback = function()
--             vim.diagnostic.open_float({ focusable = false })
--         end,
--         group = vim.api.nvim_create_augroup("DiagnosticFloat", { clear = true })
--     }
-- )

-- LSP attach actions
local lsp_attach_augroup = vim.api.nvim_create_augroup("LspAttach", { clear = true })

-- Attach Navic
vim.api.nvim_create_autocmd(
    "LspAttach",
    {
        group = lsp_attach_augroup,
        desc = "Navic",
        callback = function(args)
            local navic_status, navic = pcall(require, "nvim-navic")
            if navic_status then
                local client = vim.lsp.get_client_by_id(args.data.client_id)
                if client ~= nil and client.server_capabilities.documentSymbolProvider then
                    navic.attach(client, args.buf)
                end
            end
        end
    }
)

-- Hover doc popup
local pop_opts = { border = "rounded", max_width = 80 }
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, pop_opts)
vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, pop_opts)

-- Key mappings
vim.api.nvim_create_autocmd(
    "LspAttach",
    {
        group = lsp_attach_augroup,
        desc = "LSP keymaps",
        callback = function(args)
            local bufnr = args.buf
            local which_key = require "which-key"
            local telescope_builtin = require "telescope.builtin"
            which_key.add(
                {
                    { "<C-S-i>",    "<Cmd>Lspsaga peek_definition<CR>",                             desc = "Peek definition" },
                    { "<C-k>",      vim.lsp.buf.signature_help,                                     desc = "Show signature help" },
                    { "<F2>",       vim.diagnostic.goto_next,                                       desc = "Next diagnostic" },
                    { "<F6>",       "<Cmd>Lspsaga rename<CR>",                                      desc = "Rename (LSP)" },
                    { "<S-F2>",     vim.diagnostic.goto_prev,                                       desc = "Previous diagnostic" },
                    { "K",          vim.lsp.buf.hover,                                              desc = "Hover documentation" },
                    { "gD",         vim.lsp.buf.declaration,                                        desc = "Jump to the declaration of the symbol under the cursor" },
                    { "gR",         "<Cmd>Lspsaga finder<CR>",                                      desc = "LSP finder" },
                    { "ga",         "<Cmd>Lspsaga code_action<CR>",                                 desc = "Code action (Lspsaga)" },
                    { "gd",         vim.lsp.buf.definition,                                         desc = "Jump to the definition of the symbol under the cursor" },
                    { "gi",         vim.lsp.buf.implementation,                                     desc = "Jump to implementation" },
                    { "gr",         telescope_builtin.lsp_references,                               desc = "Show references in Telescope" },
                    { "<leader>l",  buffer = bufnr,                                                 group = "LSP" },
                    { "<leader>lF", function() vim.lsp.buf.format({ async = true }) end,            desc = "Format document" },
                    { "<leader>lI", telescope_builtin.lsp_implementations,                          desc = "Implementations" },
                    { "<leader>lS", telescope_builtin.lsp_workspace_symbols,                        desc = "Workspace symbols" },
                    { "<leader>la", "<Cmd>Lspsaga code_action<CR>",                                 desc = "Code action" },
                    { "<leader>ld", telescope_builtin.lsp_definitions,                              desc = "Definitions" },
                    { "<leader>lf", function() vim.lsp.buf.format({ async = true }) end,            desc = "Format selection",                                      mode = "v" },
                    { "<leader>li", telescope_builtin.lsp_incoming_calls,                           desc = "Incoming calls" },
                    { "<leader>ll", "<Cmd>Lspsaga show_line_diagnostics<CR>",                       desc = "Show line diagnostics" },
                    { "<leader>lo", telescope_builtin.lsp_outgoing_calls,                           desc = "Outgoing calls" },
                    { "<leader>lp", "<Cmd>Lspsaga peek_definition<CR>",                             desc = "Peek definition" },
                    { "<leader>lq", "<Cmd>lua vim.diagnostic.setloclist()<CR>",                     desc = "Quickfix" },
                    { "<leader>lr", telescope_builtin.lsp_references,                               desc = "References" },
                    { "<leader>ls", telescope_builtin.lsp_document_symbols,                         desc = "Document symbols" },
                    { "<leader>lt", telescope_builtin.lsp_type_definitions,                         desc = "Type definitions" },
                    { "<leader>o",  "<Cmd>Lspsaga outline<CR>",                                     desc = "LspSaga outline" },
                    { "<leader>th", function() vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled()) end, desc = "Toggle inlay hints" },
                }
            )
        end,
    }
)
