local lspconfig_status, lspconfig = pcall(require, "lspconfig")
if (not lspconfig_status) then
    return
end

local mason_registry = require "user.lsp.mason-registry"
local typescript_ls_pkg_name = "typescript-language-server"
if (mason_registry.ensure_installed(typescript_ls_pkg_name)) then
    -- Enable (broadcasting) snippet capability for completion
    local cmp_lsp = require "cmp_nvim_lsp"

    lspconfig.ts_ls.setup({
        capabilities = cmp_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities()),
    })
end
