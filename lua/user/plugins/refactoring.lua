return {
    "ThePrimeagen/refactoring.nvim",
    lazy = true,
    dependencies = {
        { "nvim-lua/plenary.nvim" },
        { "nvim-treesitter/nvim-treesitter" },
        { "folke/which-key.nvim" }
    },
    config = function()
        local refactoring = require "refactoring"
        refactoring.setup({})

        local which_key = require "which-key"
        local telescope_status, telescope = pcall(require, "telescope")
        if telescope_status then
            telescope.load_extension("refactoring")
            which_key.add(
                {
                    ["<leader>rr"] = { telescope.extensions.refactoring.refactors, "Select refactoring" },
                },
                {
                    mode = "v",
                    noremap = true,
                    silent = true,
                    expr = false
                }
            )
        else
            which_key.add(
                {
                    ["<leader>rr"] = { refactoring.select_refactor, "Select refactoring" },
                },
                {
                    mode = "v",
                    noremap = true,
                    silent = true,
                    expr = false
                }
            )
        end
    end,
}
