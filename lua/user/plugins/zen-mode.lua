return {
    "folke/zen-mode.nvim",
    lazy = true,
    opts = {
        plugins = {
            alacritty = {
                enabled = true,
                font = "14",
            },
            wezterm = {
                enabled = true,
                font = "+4",
            },
        },
    },
    keys = {
        { "<leader>zz", "<Cmd>ZenMode<CR>", desc = "Zen mode toggle" }
    },
}
