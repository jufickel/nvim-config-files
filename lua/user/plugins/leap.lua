-- Bidirectional search in all windows on the tab page.
local function bidi_search()
    local leap = require "leap"
    leap.leap { target_windows = vim.tbl_filter(
        function(win)
            return vim.api.nvim_win_get_config(win).focusable
        end,
        vim.api.nvim_tabpage_list_wins(0)
    ) }
end

return {
    "ggandor/leap.nvim",
    dependencies = {
        "tpope/vim-repeat"
    },
    keys = {
        {
            "s",
            bidi_search,
            mode = { "n", "x", "o" },
            desc = "Bidirectional search in all windows on the tab page."
        },
        { "<leader>h", bidi_search, desc = "Bidirectional search in all windows on the tab page." },
    },
}
