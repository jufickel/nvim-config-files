return {
    "nvim-tree/nvim-tree.lua",                    -- Filesystem navigation
    dependencies = "nvim-tree/nvim-web-devicons", -- Filesystem icons
    opts = {
        view = {
            number = true,
            relativenumber = true,
        },
    },
    keys = {
        { "<leader>e", "<Cmd>NvimTreeToggle<CR>",   desc = "NvimTree toggle" },
        { "<S-F1>",    "<Cmd>NvimTreeFindFile<CR>", desc = "NvimTree find file" }
    }
}
