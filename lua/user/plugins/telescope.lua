return {
    "nvim-telescope/telescope.nvim", -- Fuzzy finder
    dependencies = { "folke/which-key.nvim" },
    event = "VeryLazy",
    config = function()
        local telescope = require "telescope"
        local builtin = require "telescope.builtin"
        local action_layout = require "telescope.actions.layout"
        local action_state = require "telescope.actions.state"
        local actions = require "telescope.actions"

        telescope.setup({
            defaults = {
                mappings = {
                    n = {
                        ["<M-p>"] = action_layout.toggle_preview,
                    },
                    i = {
                        ["<M-p>"] = action_layout.toggle_preview,
                    },
                },
            },
            extensions = { "dap", "fzf" }
        })

        local function deletable_entries_buffers(opts)
            builtin.buffers({
                attach_mappings = function(prompt_bufnr, map)
                    local delete_buf = function()
                        local current_picker = action_state.get_current_picker(prompt_bufnr)
                        local multi_selections = current_picker:get_multi_selection()

                        if nil == next(multi_selections) then
                            local selection = action_state.get_selected_entry()
                            vim.api.nvim_buf_delete(selection.bufnr, { force = true })
                        else
                            for _, selection in ipairs(multi_selections) do
                                vim.api.nvim_buf_delete(selection.bufnr, { force = true })
                            end
                        end
                        actions.close(prompt_bufnr)

                        -- As picker has no refresh, close the picker and open it
                        -- again immediately right after.
                        deletable_entries_buffers(opts)
                    end
                    map('i', '<C-x>', delete_buf)
                    return true
                end
            })
        end

        -- Key mappings for Telescope
        local which_key = require "which-key"
        which_key.add({
            { "<leader>g",  group = "Telescope Git actions" },
            { "<leader>gb", "<Cmd>Telescope git_branches<CR>", desc = "Checkout branch" },
            { "<leader>gc", "<Cmd>Telescope git_commits<CR>",  desc = "Checkout commit" },
            { "<leader>go", "<Cmd>Telescope git_status<CR>",   desc = "Open changed file" },
            { "<leader>f",  group = "Telescope" },
            { "<leader>fb", deletable_entries_buffers,         desc = "Show buffers" },
            { "<leader>fc", "<Cmd>Telescope commands<CR>",     desc = "Commands" },
            {
                "<leader>fd",
                function() builtin.diagnostics({ bufnr = 0 }) end,
                desc = "Document diagnostics"
            },
            { "<leader>ff", "<Cmd>Telescope find_files<CR>",     desc = "Find files" },
            { "<leader>fh", "<Cmd>Telescope help_tags<CR>",      desc = "Help tags" },
            { "<leader>fk", "<Cmd>Telescope keymaps<CR>",        desc = "Keymaps" },
            { "<leader>fl", "<Cmd>Telescope live_grep<CR>",      desc = "Live grep" },
            { "<leader>fm", "<Cmd>Telescope marks<CR>",          desc = "Show marks" },
            { "<leader>fo", "<Cmd>Telescope oldfiles<CR>",       desc = "Recent files" },
            { "<leader>fq", "<Cmd>Telescope quickfix<CR>",       desc = "Quckfix" },
            { "<leader>fr", "<Cmd>Telescope registers<CR>",      desc = "Registers" },
            { "<leader>fs", "<Cmd>Telescope search_history<CR>", desc = "Search history" },
        })
    end
}
