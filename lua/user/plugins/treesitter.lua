return {
    "nvim-treesitter/nvim-treesitter",
    dependencies = {
        "nvim-treesitter/nvim-treesitter-refactor"
    },
    event = { "BufReadPost", "BufNewFile" },
    config = function()
        local treesitter_configs = require "nvim-treesitter.configs"
        treesitter_configs.setup({
            ensure_installed = {
                "bash",
                "c",
                "css",
                "dockerfile",
                "graphql",
                "html",
                "http",
                "java",
                "javascript",
                "json",
                "kotlin",
                "lua",
                "markdown",
                "markdown_inline",
                "norg",
                "python",
                "rust",
                "toml",
                "vim",
                "vimdoc",
                "yaml"
            },
            auto_install = true,
            highlight = {
                enable = true,
                additional_vim_regex_highlighting = false,
            },
            indent = { enable = true },
            -- rainbow = {
            --     enable = true,
            --     query = "rainbow-parens",
            --     strategy = require("ts-rainbow").strategy.global,
            -- },
            refactor = {
                highlight_definitions = {
                    enable = true,

                    -- Set to false if you have an `updatetime` of ~100.
                    clear_on_cursor_move = true,
                },
                highlight_current_scope = {
                    enable = false,
                },
            },
        })
    end
}
