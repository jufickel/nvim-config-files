return {
    "folke/which-key.nvim",
    dependencies = { "echasnovski/mini.icons" },
    event = "VeryLazy",
    opts = {
        plugins = {
            spelling = {
                enabled = true,
                suggestions = 20,
            },
        }
    },
}
