return {
    "mfussenegger/nvim-dap", -- Debugger
    lazy = true,
    config = function()
        local dap = require "dap"
        local which_key = require "which-key"
        which_key.add(
            {
                { "<F7>",      dap.step_into,     desc = "[Debugger] Step into" },
                { "<F8>",      dap.step_over,     desc = "[Debugger] Step over" },
                { "<S-F8>",    dap.step_out,      desc = "[Debugger] Step out" },
                { "<F9>",      dap.continue,      desc = "[Debugger] Continue" },
                { "<leader>d", group = "Debugger" },
                {
                    "<leader>dB",
                    "<cmd>lua require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>",
                    desc = "Toggle conditional breakpoint"
                },
                { "<leader>db", dap.toggle_breakpoint, desc = "Toggle breakpoint" },
                {
                    "<leader>dl",
                    "<cmd>lua require('dap').set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>",
                    desc = "Toggle logging breakpoint"
                },
                { "<leader>dr", dap.repl.open,         desc = "Open REPL" },
            }
        )

        dap.adapters.gdb = {
            type = "executable",
            command = "gdb",
            args = { "-i", "dap" },
        }

        dap.adapters.codelldb = {
            type = "server",
            port = "${port}",
            executable = {
                command = "codelldb",
                args = { "--port", "${port}" },
            }
        }

        dap.configurations.rust = {
            {
                name = "Launch",
                type = "gdb",
                request = "launch",
                program = function()
                    return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/", "file")
                end,
                cwd = "${workspaceFolder}",
                stopAtBeginningOfMainsubprogram = false,
            }
        }
    end,
}
