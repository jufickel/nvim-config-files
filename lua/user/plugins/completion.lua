vim.opt.completeopt = { "menuone", "noinsert" }

return {
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-nvim-lua",
        "hrsh7th/cmp-nvim-lsp", -- LSP completion source
        "hrsh7th/cmp-nvim-lsp-signature-help",
        "hrsh7th/cmp-path",
        "f3fora/cmp-spell",
        "onsails/lspkind.nvim",     -- Shows icons in cmp
        "L3MON4D3/LuaSnip",         -- Snippets for completion
        "saadparwaiz1/cmp_luasnip", -- Snippets for completion
        "rafamadriz/friendly-snippets",
    },
    config = function()
        local luasnip = require("luasnip")
        require("luasnip/loaders/from_vscode").lazy_load()
        local lspkind = require("lspkind")
        lspkind.init()

        local has_words_before = function()
            local line, col = unpack(vim.api.nvim_win_get_cursor(0))
            return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
        end

        local cmp = require("cmp")
        cmp.setup({
            performance = {
                trigger_debounce_time = 1000,
            },
            snippet = {
                expand = function(args)
                    luasnip.lsp_expand(args.body) -- For `luasnip` users.
                end,
            },
            mapping = {
                ["<Up>"] = cmp.mapping.select_prev_item(),
                ["<C-p>"] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
                ["<Down>"] = cmp.mapping.select_next_item(),
                ["<C-n>"] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
                ["<C-f>"] = cmp.mapping.scroll_docs(4),
                ["<C-S-f>"] = cmp.mapping.scroll_docs(-4),
                ["<C-Space>"] = cmp.mapping.complete(),
                ["<ESC>"] = cmp.mapping.abort(),
                ["<CR>"] = cmp.mapping.confirm { select = false },
                ["<S-CR>"] = cmp.mapping.confirm {
                    behavior = cmp.ConfirmBehavior.Replace,
                    select = false
                },
                ["<Tab>"] = cmp.mapping(
                    function(fallback)
                        if cmp.visible() then
                            cmp.select_next_item()
                        elseif luasnip.expand_or_jumpable() then
                            luasnip.expand_or_jump()
                        elseif has_words_before() then
                            cmp.complete()
                        else
                            fallback()
                        end
                    end,
                    { "i", "s" }
                ),
                ["<S-Tab>"] = cmp.mapping(
                    function(fallback)
                        if cmp.visible() then
                            cmp.select_prev_item()
                        elseif luasnip.jumpable(-1) then
                            luasnip.jump(-1)
                        else
                            fallback()
                        end
                    end,
                    { "i", "s" }
                ),
            },
            sources = {
                { name = "nvim_lsp",               priority = 50 }, -- from language server
                { name = "nvim_lsp_signature_help" },               -- display function signatures with current parameter emphasized
                { name = "nvim_lua" },
                { name = "luasnip" },                               -- for lua users
                { name = "buffer" },                                -- source current buffer
                { name = "path" },                                  -- file paths
                { name = "spell" },                                 -- based on Vim's spellsuggest
                { name = "treesitter" },
                { name = "neorg" },
            },
            preselect = cmp.PreselectMode.None,
            confirmation = {
                get_commit_characters = function(_)
                    return {}
                end
            },
        })
    end
}
