return {
    "gbrlsnchs/winpick.nvim", -- Show visual clues with labels to select a window
    dependencies = { "folke/which-key.nvim" },
    lazy = true,
    opts = {},
    keys = {
        { "<C-W>p",
            function()
                local win_id = require("winpick").select()
                if win_id then
                    vim.api.nvim_set_current_win(win_id)
                end
            end,
            "Select window"
        },
    },
}
