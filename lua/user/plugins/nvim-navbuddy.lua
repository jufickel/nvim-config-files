return {
    "SmiteshP/nvim-navbuddy",
    dependencies = {
        "SmiteshP/nvim-navic",
        "MunifTanjim/nui.nvim",
    },
    opts = {
        lsp = { auto_attach = true },
        window = {
            size = {
                height = "70%",
                width = "100%",
            },
        },
    },
    keys = {
        { "<leader><leader>", "<Cmd>Navbuddy<CR>", desc = "Navbuddy" },
    },
}
