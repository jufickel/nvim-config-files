return {
    {
        "m4xshen/autoclose.nvim", -- Autocomplete (), {}, []
        event = "VeryLazy",
        opts = {
            options = {
                -- Do not autoclose when cursor touches alphanumeric characters
                -- or `(` or `[` or `{`
                disable_when_touch = true,
                touch_regex = "[%w(%[{]",
            },
        },
    },
    {
        "rainbowhxch/beacon.nvim",
        event = "VeryLazy",
    },
    {
        "tjdevries/colorbuddy.vim",
        event = "VeryLazy",
    },
    {
        "numToStr/Comment.nvim", -- Commenting plugin
        event = { "BufReadPre", "BufNewFile" },
        opts = {},
    },
    {
        "sharkdp/fd", -- Finder
        lazy = true,
    },
    {
        "j-hui/fidget.nvim", -- Show status of nvim-lsp progress
        event = "VeryLazy",
        opts = {},
    },
    {
        "vhyrro/luarocks.nvim",
        priority = 1000, -- Very high priority is required, luarocks.nvim should run as the first plugin in your config.
        config = true,
        opts = { "lua-curl", "nvim-nio", "mimetypes", "xml2lua" },
    },
    {
        "williamboman/mason.nvim",
        cmd = "Mason",
        dependencies = { "williamboman/mason-lspconfig.nvim" },
        opts = {},
    },
    {
        "Canop/nvim-bacon",
        event = "LspAttach",
        ft = { "rust" },
        opts = {
            quickfix = {
                enabled = true,
                event_trigger = true,
            },
        },
    },
    {
        "theHamsta/nvim-dap-virtual-text",
        lazy = true,
        opts = {},
    },
    {
        "mfussenegger/nvim-jdtls",
        lazy = true,
    },
    {
        "gennaro-tedesco/nvim-jqx",
        ft = { "json", "yaml" },
    },
    {
        "neovim/nvim-lspconfig", -- Collection of common configurations for the Neovim LSP client
        event = { "BufReadPre", "BufNewFile" },
    },
    {
        "kylechui/nvim-surround",
        event = { "BufReadPre", "BufNewFile" },
        opts = {
            surrounds = {
                ["("] = false,
                ["["] = false,
            },
            aliases = {
                ["("] = ")",
                ["["] = "]",
            },
        },
    },
    {
        "navarasu/onedark.nvim",
        lazy = false,
        priority = 1000,
    },
    {
        "nvim-lua/plenary.nvim",
        lazy = true,
    },
    {
        "nvim-lua/popup.nvim",
        lazy = true,
    },
    {
        "BurntSushi/ripgrep", -- Telescope grep
        lazy = true,
    },
    {
        "vinnymeller/swagger-preview.nvim",
        build = "npm -g install swagger-ui-watcher --prefix ~/.local",
        opts = {},
        ft = { "yaml" },
    },
    {
        "nvim-telescope/telescope-fzf-native.nvim", -- Make Telescope faster
        lazy = true,
        build = "make",
    },
    {
        "mbbill/undotree",
        lazy = true,
        keys = {
            { "<leader>u", "<Cmd>UndotreeToggle<CR>", desc = "Undo Tree toggle" },
        }
    },
}
