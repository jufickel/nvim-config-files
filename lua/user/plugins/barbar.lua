local silent_noremap = { noremap = true, silent = true }

return {
    "romgrk/barbar.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    init = function()
        vim.g.barbar_auto_setup = false
    end,
    lazy = false,
    opts = {},
    keys = {

        -- Move to previous/next
        { "<A-,>",      "<Cmd>BufferPrevious<CR>",            silent_noremap, desc = "Previous buffer" },
        { "<A-.>",      "<Cmd>BufferNext<CR>",                silent_noremap, desc = "Next buffer" },

        -- Re-order to previous/next
        { "<A-<>",      "<Cmd>BufferMovePrevious<CR>",        silent_noremap, desc = "Move buffer previous" },
        { "<A->>",      "<Cmd>BufferMoveNext<CR>",            silent_noremap, desc = "Move buffer next" },

        -- Go to buffer in position…
        { "<A-1>",      "<Cmd>BufferGoto 1<CR>",              silent_noremap, desc = "Go to first buffer" },
        { "<A-2>",      "<Cmd>BufferGoto 2<CR>",              silent_noremap, desc = "Go to second buffer" },
        { "<A-3>",      "<Cmd>BufferGoto 3<CR>",              silent_noremap, desc = "Go to third buffer" },
        { "<A-4>",      "<Cmd>BufferGoto 4<CR>",              silent_noremap, desc = "Go to fourth buffer" },
        { "<A-5>",      "<Cmd>BufferGoto 5<CR>",              silent_noremap, desc = "Go to fifth buffer" },
        { "<A-6>",      "<Cmd>BufferGoto 6<CR>",              silent_noremap, desc = "Go to sixth buffer" },
        { "<A-7>",      "<Cmd>BufferGoto 7<CR>",              silent_noremap, desc = "Go to seventh buffer" },
        { "<A-8>",      "<Cmd>BufferGoto 8<CR>",              silent_noremap, desc = "Go to eigth buffer" },
        { "<A-9>",      "<Cmd>BufferGoto 9<CR>",              silent_noremap, desc = "Go to ninth buffer" },
        { "<A-0>",      "<Cmd>BufferLast<CR>",                silent_noremap, desc = "Go to last buffer" },

        -- Pin/unpin buffer
        { "<A-p>",      "<Cmd>BufferPin<CR>",                 silent_noremap, desc = "Pin buffer" },

        -- Close buffer
        { "<A-c>",      "<Cmd>BufferClose<CR>",               silent_noremap, desc = "Close buffer" },

        -- Magic buffer-picking mode
        { "<C-p>",      "<Cmd>BufferPick<CR>",                silent_noremap, desc = "Pick buffer" },

        -- Sort automatically by …
        { "<leader>bb", "<Cmd>BufferOrderByBufferNumber<CR>", silent_noremap, desc = "Order buffers by number" },
        { "<leader>bd", "<Cmd>BufferOrderByDirectory<CR>",    silent_noremap, desc = "Order buffers by directory" },
        { "<leader>bl", "<Cmd>BufferOrderByLanguage<CR>",     silent_noremap, desc = "Order buffers by language" },
        { "<leader>bw", "<Cmd>BufferOrderByWindowNumber<CR>", silent_noremap, desc = "Order buffers by window number" },
    }
}
