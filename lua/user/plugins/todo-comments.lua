return {
    "folke/todo-comments.nvim",
    cmd = { "TodoTelescope" },
    event = { "BufReadPost", "BufNewFile" },
    dependencies = { "nvim-lua/plenary.nvim" },
    opts = {},
    keys = {
        {
            "]t",
            function()
                require("todo-comments").jump_next()
            end,
            desc = "Next TODO comment"
        },
        {
            "[t",
            function()
                require("todo-comments").jump_prev()
            end,
            desc = "Previous TODO comment"
        },
        {
            "<leader>xt",
            "<Cmd>TodoLocList<CR>",
            desc = "Show TODOs in Trouble"
        },
        {
            "<leader>xT",
            "<Cmd>TodoQuickFix<CR>",
            desc = "Todo/Fix/Fixme (Trouble)"
        },
        {
            "<leader>ft",
            "<Cmd>TodoTelescope<CR>",
            desc = "Show TODOs in Telescope"
        },
        {
            "<leader>fT",
            "<~md>TodoTelescope keywords=TODO,FIX,FIXME<cr>",
            desc = "Todo/Fix/Fixme"
        },
    },
}
