return {
    "goolord/alpha-nvim", -- fast and fully programmable greeter
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
        require("alpha").setup(require("alpha.themes.dashboard").config)
    end
}
