return {
    "folke/trouble.nvim", -- Summarizes issues
    opts = {},
    cmd = "Trouble",
    keys = {
        {
            "<leader>xx",
            "<Cmd>Trouble diagnostics toggle focus=true<CR>",
            desc = "Document Diagnostics (Trouble)"
        },
        {
            "<leader>xX",
            "<Cmd>Trouble diagnostics toggle focus=true filter.buf=0<CR>",
            desc = "Buffer Diagnostics (Trouble)"
        },
        {
            "<leader>xL",
            "<Cmd>Trouble loclist toggle<CR>",
            desc = "Location List (Trouble)"
        },
        {
            "<leader>xQ",
            "<Cmd>Trouble qflist toggle<CR>",
            desc = "Quickfix List (Trouble)"
        },
        {
            "[q",
            function()
                local trouble = require "trouble"
                if trouble.is_open() then
                    trouble.previous({ skip_groups = true, jump = true })
                else
                    vim.cmd.cprev()
                end
            end,
            desc = "Previous trouble/quickfix item",
        },
        {
            "]q",
            function()
                local trouble = require "trouble"
                if trouble.is_open() then
                    trouble.next({ skip_groups = true, jump = true })
                else
                    vim.cmd.cnext()
                end
            end,
            desc = "Next trouble/quickfix item",
        },
    },
}
