-- A simple statusline/winbar component that uses LSP to show your current code context.
return {
    "SmiteshP/nvim-navic",
    lazy = true,
    dependencies = "neovim/nvim-lspconfig",
    opts = {
        highlight = true,
    },
}
