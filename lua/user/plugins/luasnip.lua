return {
    "L3MON4D3/LuaSnip",
    dependencies = {
        "rafamadriz/friendly-snippets",
        config = function()
            local from_vscode_loader = require("luasnip.loaders.from_vscode")
            from_vscode_loader.lazy_load({ paths = "./snippets" }) -- refers to $HOME/.config/nvim/snippets
        end,
    },
    config = function()
        local luasnip = require "luasnip"
        local types = require "luasnip.util.types"
        luasnip.config.setup({
            history = true,
            delete_check_events = "TextChanged", -- keep around last snippet local to jump back
            enable_autosnippets = true,
            updateevents = "TextChanged,TextChangedI",
            ext_opts = {
                -- This adds an orange dot to the end of the line if inside a
                -- `choiceNode`, and a blue one if inside an `insertNode`:
                [types.choiceNode] = {
                    active = {
                        virt_text = { { "●", "Character" } }
                    }
                },
                [types.insertNode] = {
                    active = {
                        virt_text = { { "●", "Directory" } }
                    }
                }
            },
        })
    end,
    -- stylua: ignore
    keys = {
        {
            "<Tab>",
            function()
                return require("luasnip").jumpable(1) and "<Plug>luasnip-jump-next" or "<tab>"
            end,
            expr = true,
            silent = true,
            mode = "i",
        },
        { "<Tab>",   function() require("luasnip").jump(1) end,  mode = "s" },
        { "<S-Tab>", function() require("luasnip").jump(-1) end, mode = { "i", "s" } },
    },
}
