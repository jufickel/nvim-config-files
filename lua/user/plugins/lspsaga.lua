return {
    "nvimdev/lspsaga.nvim",
    event = "LspAttach",
    lazy = true,
    opts = {
        symbol_in_winbar = {
            in_custom = true,
        },
        ui = {
            code_action = "\u{ea61}",
        },
        rename = {
            in_select = false,
        },
        -- your configuration
        code_action = {
            quit = "<Esc>",
        },
    },
    dependencies = {
        "nvim-tree/nvim-web-devicons",
        "nvim-treesitter/nvim-treesitter",
    }
}
