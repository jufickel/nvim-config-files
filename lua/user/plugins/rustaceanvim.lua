return {
    "mrcjkb/rustaceanvim", -- rust-analyzer hints and more!
    version = "^5",
    lazy = false,
    ft = "rust",
    dependencies = {
        "williamboman/mason.nvim",
        "folke/which-key.nvim",
        "neovim/nvim-lspconfig",
    },
    config = function()
        local on_attach = function(_, _)
            local which_key = require "which-key"
            which_key.add({
                { "<C-Space>",   function() vim.cmd.RustLsp { "hover", "actions" } end,             desc = "Rust hover actions" },
                { "<Leader>gdr", function() vim.cmd.RustLsp("relatedDiagnostics") end,              desc = "Jumpt to related diagnostics" },
                { "<Leader>rd",  function() vim.cmd.RustLsp("debuggables") end,                     desc = "Rust debuggables" },
                { "<Leader>rfc", function() vim.cmd.RustLsp("flyCheck") end,                        desc = "Rust fly check" },
                { "<Leader>roc", function() vim.cmd.RustLsp("openCargo") end,                       desc = "Rust open Cargo.toml" },
                { "<Leader>rod", function() vim.cmd.RustLsp("openDocs") end,                        desc = "Rust open docs.rs documentation for the symbol under the cursor" },
                { "<Leader>rp",  function() vim.cmd.RustLsp({ "renderDiagnostic", "current" }) end, desc = "Rust render current diagnostics" },
                { "<Leader>rr",  function() vim.cmd.RustLsp("runnables") end,                       desc = "Rust runnables" },
                { "ga",          function() vim.cmd.RustLsp("codeAction") end,                      desc = "Rust code actions" },
            })
        end

        vim.g.rustaceanvim = {
            inlay_hints = {
                highlight = "NonText",
            },
            tools = {

            },
            server = {
                capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities()),
                on_attach = function(client, bufnr)
                    on_attach(client, bufnr)
                end,
                settings = {
                    ["rust-analyzer"] = {
                        checkOnSave = {
                            command = "clippy"
                        },
                    }
                },
                standalone = true,
            },
        }
    end,
}
