return {
    "mfussenegger/nvim-lint",
    lazy = true,
    event = "LspAttach",
    config = function()
        local mason_registry = require "user.lsp.mason-registry"

        -- Markdown linters
        local markdown_linters = {}
        local markdownlint_pkg_name = "markdownlint"
        if (mason_registry.ensure_installed(markdownlint_pkg_name)) then
            table.insert(markdown_linters, markdownlint_pkg_name)
        end

        -- XML linters
        local xml_linters = {}
        local vale_pkg_name = "vale"
        if (mason_registry.ensure_installed(vale_pkg_name)) then
            table.insert(xml_linters, vale_pkg_name)
        end

        -- YAML linters
        local yaml_linters = {}
        local yamllint_pkg_name = "yamllint"
        if (mason_registry.ensure_installed(yamllint_pkg_name)) then
            table.insert(yaml_linters, yamllint_pkg_name)
        end

        local lint = require "lint"
        lint.linters_by_ft = {
            markdown = markdown_linters,
            xml = xml_linters,
            yaml = yaml_linters,
        }

        -- Setup a autocmd to trigger linting
        vim.api.nvim_create_autocmd(
            { "BufWritePost" },
            {
                callback = function()
                    lint.try_lint()
                end,
            }
        )
    end
}
