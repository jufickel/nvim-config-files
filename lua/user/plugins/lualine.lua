return {
    "nvim-lualine/lualine.nvim", -- Statusline and winbar
    event = "VeryLazy",
    dependencies = {
        "nvim-tree/nvim-web-devicons",
        "SmiteshP/nvim-navic",
    },
    opts = {
        options = {
            theme = "onedark",
            disabled_filetypes = {
                winbar = { "alpha", "help", "NvimTree", "tagbar" },
            },
        },
        extensions = { "fzf", "lazy", "nvim-dap-ui", "nvim-tree", "quickfix", "trouble" },
    }
}
