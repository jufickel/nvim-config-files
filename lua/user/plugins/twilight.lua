return {
    "folke/twilight.nvim",
    lazy = true,
    opts = {},
    keys = {
        { "<leader>zt", "<Cmd>Twilight<CR>", "Twilight toggle" },
    },
}
