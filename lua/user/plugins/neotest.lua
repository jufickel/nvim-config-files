return {
    "nvim-neotest/neotest",
    dependencies = {
        "nvim-neotest/nvim-nio",
        "nvim-lua/plenary.nvim",
        "antoinemadec/FixCursorHold.nvim",
        "nvim-treesitter/nvim-treesitter",
        "vim-test/vim-test",
        "nvim-neotest/neotest-vim-test",
    },
    lazy = true,
    ft = { "java", "kotlin", "rust" },
    config = function()
        local neotest = require "neotest"
        neotest.setup({
            adapters = {
                require("rustaceanvim.neotest"),
                require("neotest-vim-test")({
                    allow_file_types = { "java", "kotlin", },
                }),
            },
        })

        local commands = {
            {
                name = "NeotestRunNearest",
                opts = {
                    desc = "require('neotest').run.run()"
                },
                command = neotest.run.run
            },
            {
                name = "NeotestRunCurrentFile",
                opts = {
                    desc = "require('neotest').run.run(vim.fn.expand('%'))"
                },
                command = function()
                    neotest.run.run(vim.fn.expand("%"))
                end
            },
            {
                name = "NeotestDebugNearest",
                opts = {
                    desc = "require('neotest').run.run({strategy = 'dap'})"
                },
                command = function()
                    neotest.run.run({ strategy = "dap" })
                end
            },
            {
                name = "NeotestStopNearest",
                opts = {
                    desc = "require('neotest').run.stop()"
                },
                command = neotest.run.stop
            },
            {
                name = "NeotestSummaryToggle",
                opts = {
                    desc = "require('neotest').summary.toggle()"
                },
                command = neotest.summary.toggle
            },
            {
                name = "NeotestAttachToNearest",
                opts = {
                    desc = "require('neotest').run.attach()"
                },
                command = neotest.run.attach
            },
            {
                name = "NeotestOutputOpen",
                opts = {
                    desc = "Open the output of a test result"
                },
                command = function()
                    neotest.output.open()
                end
            },
        }

        for _, cmd in ipairs(commands) do
            vim.api.nvim_create_user_command(cmd.name, cmd.command, cmd.opts)
        end
    end,
    keys = {
        { "<leader>tdn", "<Cmd>NeotestDebugNearest<CR>",   desc = "Debug nearest test in Neotest" },
        { "<leader>trn", "<Cmd>NeotestRunNearest<CR>",     desc = "Run nearest test in Neotest" },
        { "<leader>trf", "<Cmd>NeotestRunCurrentFile<CR>", desc = "Test current file in Neotest" },
        { "<leader>ts",  "<Cmd>NeotestSummaryToggle<CR>",  desc = "Toggle Neotest summary" },
    }
}
