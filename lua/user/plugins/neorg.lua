return {
    "nvim-neorg/neorg",
    lazy = false,
    dependencies = { "luarocks.nvim" },
    version = "*",
    config = function()
        require("neorg").setup {
            load = {
                ["core.defaults"] = {},
                ["core.completion"] = {
                    config = {
                        engine = "nvim-cmp",
                    },
                },
                ["core.concealer"] = {},
                ["core.dirman"] = {
                    config = {
                        workspaces = {
                            Notizen = "~/Notizen",
                        },
                        default_workspace = "Notizen",
                    },
                },
                ["core.integrations.nvim-cmp"] = {},
            },
        }

        vim.wo.foldlevel = 99
        vim.wo.conceallevel = 2
    end,
}
