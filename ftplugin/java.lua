local home = os.getenv("HOME")
local jdtls_status, jdtls = pcall(require, "jdtls")
if not jdtls_status then
    vim.notify("jdtls is not installed.", vim.log.levels.WARN)
    return
end

-- File types that signify a Java project's root directory. This will be
-- used by eclipse to determine what constitutes a workspace
local root_markers = { "gradlew", "mvnw", ".git" }
local root_dir = require("jdtls.setup").find_root(root_markers)
local java_home = os.getenv("JAVA_HOME")
if not java_home then
    vim.notify("Could not find JAVA_HOME.", vim.log.levels.WARN)
    return
end


local jdtls_location = vim.fn.stdpath("data") .. "/mason/packages/jdtls"

-- Only for Linux and Mac
local system = "linux"
if vim.fn.has("mac") == 1 then
    system = "mac"
end

-- eclipse.jdt.ls stores project specific data within a folder. If you are working
-- with multiple different projects, each project must use a dedicated data directory.
-- This variable is used to configure eclipse to use the directory name of the
-- current project found using the root_marker as the folder for project specific data.
local workspace_folder = home .. "/.local/share/eclipse/" .. vim.fn.fnamemodify(root_dir, ":p:h:t")

local on_attach = function(_, _)
    local which_key = require "which-key"
    which_key.add(
        {
            ["<A-o"] = { jdtls.organize_imports, "Organise imports" },
            ["crv"] = { jdtls.extract_variable, "Extract variable" },
            ["crc"] = { jdtls.extract_constant, "Extract constant" },
        },
        { mode = "n" }
    )
    which_key.add(
        {
            ["crv"] = { "<Esc><Cmd>lua require('jdtls').extract_variable(true)<CR>", "Extract variable" },
            ["crc"] = { "<Esc><Cmd>lua require('jdtls').extract_constant(true)<CR>", "Extract constant" },
            ["crm"] = { "<Esc><Cmd>lua require('jdtls').extract_method(true)<CR>", "Extract method" },
        },
        { mode = "v" }
    )

    -- With `hotcodereplace = 'auto' the debug adapter will try to apply code changes
    -- you make during a debug session immediately.
    -- Remove the option if you do not want that.
    -- You can use the `JdtHotcodeReplace` command to trigger it manually
    jdtls.setup_dap({ hotcodereplace = "auto" })
end

-- The nvim-cmp supports additional LSP's capabilities so we need to
-- advertise it to LSP servers:
local cmp_nvim_lsp = require("cmp_nvim_lsp")
local capabilities = cmp_nvim_lsp.default_capabilities(vim.lsp.protocol.make_client_capabilities())
local java_dap_location = vim.fn.stdpath("data") .. "/mason/packages/java-debug-adapter"
local bundles = {
    vim.fn.glob(java_dap_location .. "/extension/server/com.microsoft.java.debug.plugin-*.jar", 1)
}
local java_test_location = vim.fn.stdpath("data") .. "/mason/packages/java-test"
vim.list_extend(bundles, vim.split(vim.fn.glob(java_test_location .. "/extension/server/*.jar", 1), "\n"))

local config = {
    on_attach = on_attach,
    init_options = {
        bundles = bundles
    },
    flags = {
        debounce_text_changes = 120,
    },
    capabilities = capabilities,
    root_dir = root_dir,
    -- Here you can configure eclipse.jdt.ls specific settings
    -- These are defined by the eclipse.jdt.ls project and will be passed to eclipse when starting.
    -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
    -- for a list of options
    settings = {
        java = {
            signatureHelp = { enabled = true },
            contentProvider = { preferred = "fernflower" }, -- Use fernflower to decompile library code
            -- Specify any completion options
            completion = {
                favoriteStaticMembers = {
                    "org.hamcrest.MatcherAssert.assertThat",
                    "org.hamcrest.Matchers.*",
                    "org.hamcrest.CoreMatchers.*",
                    "org.junit.jupiter.api.Assertions.*",
                    "java.util.Objects.requireNonNull",
                    "java.util.Objects.requireNonNullElse",
                    "org.mockito.Mockito.*"
                },
                filteredTypes = {
                    "com.sun.*",
                    "io.micrometer.shaded.*",
                    "java.awt.*",
                    "jdk.*", "sun.*",
                },
            },
            -- Specify any options for organizing imports
            sources = {
                organizeImports = {
                    starThreshold = 9999,
                    staticStarThreshold = 9999,
                },
            },
            -- How code generation should act
            codeGeneration = {
                toString = {
                    template = "${object.className}{${member.name()}=${member.value}, ${otherMembers}}"
                },
                hashCodeEquals = {
                    useJava7Objects = true,
                },
                useBlocks = true,
            },
            -- If you are developing in projects with different Java versions, you need
            -- to tell eclipse.jdt.ls to use the location of the JDK for your Java version
            -- See https://github.com/eclipse/eclipse.jdt.ls/wiki/Running-the-JAVA-LS-server-from-the-command-line#initialize-request
            -- And search for `interface RuntimeOption`
            -- The `name` is NOT arbitrary, but must match one of the elements from `enum ExecutionEnvironment` in the link above
            configuration = {
                runtimes = {
                    {
                        name = "JavaSE-19",
                        path = java_home .. "/../java-19-openjdk",
                    },
                    {
                        name = "JavaSE-17",
                        path = java_home .. "/../java-17-openjdk",
                    },
                    {
                        name = "JavaSE-11",
                        path = java_home .. "/../java-11-openjdk",
                    },
                    {
                        name = "JavaSE-1.8",
                        path = java_home .. "/../java-8-openjdk",
                    },
                }
            }
        }
    },
    -- cmd is the command that starts the language server. Whatever is placed
    -- here is what is passed to the command line to execute jdtls.
    -- Note that eclipse.jdt.ls must be started with a Java version of 17 or higher
    -- See: https://github.com/eclipse/eclipse.jdt.ls#running-from-the-command-line
    -- for the full list of options
    cmd = {
        java_home .. "/bin/java",
        "-Declipse.application=org.eclipse.jdt.ls.core.id1",
        "-Dosgi.bundles.defaultStartLevel=4",
        "-Declipse.product=org.eclipse.jdt.ls.core.product",
        "-Dlog.protocol=true",
        "-Dlog.level=ALL",
        "-Xmx4g",
        "--add-modules=ALL-SYSTEM",
        "--add-opens", "java.base/java.util=ALL-UNNAMED",
        "--add-opens", "java.base/java.lang=ALL-UNNAMED",

        -- The jar file is located where jdtls was installed.
        -- This will need to be updated to the location where you installed jdtls
        "-jar", vim.fn.glob(jdtls_location .. "/plugins/org.eclipse.equinox.launcher_*.jar"),

        -- The configuration for jdtls is also placed where jdtls was installed. This will
        -- need to be updated depending on your environment
        "-configuration", jdtls_location .. "/config_" .. system,

        -- Use the workspace_folder defined above to store data for this project
        "-data", workspace_folder,
    },
}

-- local config = {
--     cmd = { jdtls_location },
--     root_dir = vim.fs.dirname(vim.fs.find({ ".gradlew", ".git", "mvnw" }, { upward = true })[1]),
-- }

-- Finally, start jdtls. This will run the language server using the configuration we specified,
-- setup the keymappings, and attach the LSP client to the current buffer
jdtls.start_or_attach(config)
