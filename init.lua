------------
-- Leader --
------------
-- These keybindings need to be defined before the first / is called; otherwise, it will default to "\"
-- keymap.map("", "<Space>", "<Nop>")
vim.api.nvim_set_keymap("", "<Space>", "<Nop>", { noremap = true, silent = true })
vim.g.mapleader = " "      -- works across all nvim files
vim.g.maplocalleader = "," -- for specific file type

vim.g.loaded = 1
vim.g.loaded_netrwPlugin = 1

-- The order of the require statements has an effect
require "user.lazy"
require "user.keys"
require "user.colorscheme"
require "user.options"
require "user.lsp"

-- Automatically jump to last place visited in a file before exiting
vim.api.nvim_create_autocmd("BufReadPost", {
    desc = "Open file at the last position it was edited earlier.",
    -- group = misc_augroup,
    pattern = "*",
    command = "silent! normal! g`'zv"
})

-- Colour indentations higher than four levels in an appalling red.
vim.cmd.highlight("Overnesting guibg=#E06C75")
vim.fn.matchadd("Overnesting", ("\\t"):rep(5))

---------
-- GUI --
---------
vim.o.guifont = "FiraCode Nerd Font Mono:h11"
