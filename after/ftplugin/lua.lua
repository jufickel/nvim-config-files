-- Format on write Lua files
vim.api.nvim_create_autocmd(
    "BufWritePre",
    {
        pattern = "<buffer>",
        callback = function()
            vim.lsp.buf.format({ async = false })
        end
    }
)
