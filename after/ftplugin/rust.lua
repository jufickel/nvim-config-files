vim.opt.colorcolumn = "100"

-- Format on write Rust files
vim.api.nvim_create_autocmd(
    "BufWritePre",
    {
        pattern = "<buffer>",
        callback = function()
            vim.lsp.buf.format({ async = false })
        end
    }
)
